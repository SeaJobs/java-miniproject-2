package com.thary.miniproject2.Controller;

import com.thary.miniproject2.Model.Post;
import com.thary.miniproject2.Service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.Objects;

@Controller
public class PostController {
    @Autowired
    private PostService postService;

    @GetMapping()
    public String index(Model model) {
        List<Post> post = postService.getAllPosts();
        model.addAttribute("post", post);
        return "index";
    }

    @GetMapping("/create")
    public String addPost(Model model) {
        model.addAttribute("post", new Post());
        return "create";
    }

//    @PostMapping("/create")
//    public String addPost(@ModelAttribute Post post,
//                                @RequestParam("file") MultipartFile file) {
//        post.setImageURL(filePath(file));
//        postService.addPost(post);
//        return "redirect:/";
//    }

//    private String filePath(MultipartFile file) {
//        String fileName = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));
//        try {
//            File directory = new File("src/main/resources/files");
//            if (!directory.exists()) {
//                directory.mkdir();
//            }
//            Path path = Paths.get("src/main/resources/files/" + fileName);
//            Files.copy(file.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);
//            return "/files/" + path.getFileName().toString();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        return null;
//    }

}
