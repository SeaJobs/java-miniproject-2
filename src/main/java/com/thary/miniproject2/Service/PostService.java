package com.thary.miniproject2.Service;

import com.thary.miniproject2.Model.Post;

import java.util.List;

public interface PostService {
    List<Post> getAllPosts();
    Post findPostById(int id);
    Post deletePostById(int id);
    void addPost( Post post);
    void editPost (Post post);

}
