package com.thary.miniproject2.Model;

import lombok.*;
@Getter
@Setter
@ToString
public class Post {
    private int id;
    private String title;
    private String description;
    private String imageURL;
}
